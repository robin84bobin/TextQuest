﻿using Assets.Scripts;
using Assets.Scripts.Events;
using Assets.Scripts.Events.CustomEvents;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessengerWindow : MonoBehaviour {

    [SerializeField] private ScrollPicker _scrollPicker;
    [SerializeField] private VariantButton[] _variantButtons;

    List<MessageData> messages = new List<MessageData>();

    // Use this for initialization
    void Start()
    {
        ResetView();
        EventManager.Get<DataInitCompleteEvent>().Subscribe(Init);
    }

    private void ResetView()
    {
        for (int i = 0; i < _variantButtons.Length; i++)
        {
            _variantButtons[i].ResetView();
        }
    }

    private IEnumerator DelayInit()
    {
        yield return new WaitForSeconds(2f);
        Init();
    }

    private void Init()
    {
        EventManager.Get<DataInitCompleteEvent>().Unsubscribe(Init);
        EventManager.Get<NewMessageEvent>().Subscribe(ShowNewMessage);
        EventManager.Get<TypeMessageCompleteEvent>().Subscribe(OnTypeMessageComplete);
        InitMessages();
        _scrollPicker.Init();

        _scrollPicker.SetData(messages);

        if (messages.Count > 0)
        {
            InitVariantButtons(messages[messages.Count - 1]);
        }
        else
        {
            Main.Inst.userStepsController.GoToStep("start");
        }
       
    }

    private void OnTypeMessageComplete(MessageData messageData)
    {
        InitVariantButtons(messageData);
        ShowNextMessage();
    }

    public void Restart()
    {
        Main.Inst.dataController.ClearUserData();

        messages.Clear();
        _scrollPicker.SetData(messages);
        Main.Inst.userStepsController.GoToStep("start");
    }

    private void InitMessages()
    {
        IEnumerable<MessageData> items = Main.Inst.dataController.Storage<MessageData>().GetItemsList();
        messages.AddRange(items);
        
    }

    private void ShowNewMessage(MessageData messageData)
    {
        messageData.isNew = true;
        if (messageQueue.Count <= 0)
        {
            ShowMessage(messageData);
        }
        else
        messageQueue.Enqueue(messageData);

    }

    private void ShowMessage(MessageData messageData)
    {
        if (messageData == null)
        {
            return;
        }
        _scrollPicker.AddDataItem(messageData, true);
        InitVariantButtons(messageData);
    }

    Queue<MessageData> messageQueue = new Queue<MessageData>();
    private void ShowNextMessage()
    {
        if (messageQueue.Count <= 0)
        {
            return;
        }
        MessageData messageData = messageQueue.Dequeue();
        ShowMessage(messageData);
    }

    private void InitVariantButtons(MessageData messageData)
    {
        //init variant buttons
        ResetVariantButtons();
        if (!messageData.isUserMsg)
        {
            QuestMessageStepData questStep = (QuestMessageStepData)Main.Inst.dataController.Get<BaseQuestStepData>(messageData.parentQuestStepId);
            for (int i = 0; i < _variantButtons.Length; i++)
            {
                if (i < questStep.variants.Length)
                    _variantButtons[i].SetData(questStep.variants[i]);
            }
        }
    }

    private void ResetVariantButtons()
    {
        for (int i = 0; i < _variantButtons.Length; i++)
            _variantButtons[i].ResetView();
    }
}
