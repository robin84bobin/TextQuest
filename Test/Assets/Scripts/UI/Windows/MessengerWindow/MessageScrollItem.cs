﻿using Assets.Scripts.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageScrollItem : BaseScrollItem {

    public UIWidget container;
    public UIWidget backCommon;
    public UIWidget backUser;
    public UILabel message;

    MessageData data;



    public override void UpdateView(object dataObject)
    {
        StopAllCoroutines();

        data = (MessageData)dataObject;
        message.text = data.text;
        if (data.isNew && !data.isUserMsg)
        {
            data.isNew = false;
            StartCoroutine(TypeMessage(data.text));
        }
        else
        {
            message.text = data.text;
            //EventManager.Get<TypeMessageCompleteEvent>().Publish(data);
        }
       
        SetMode(data.isUserMsg);
    }

    private void SetMode(bool isUser)
    {
        backCommon.gameObject.SetActive(!isUser);
        backUser.gameObject.SetActive(isUser);
    }

    public override void SetEmpty(bool isEmpty)
    {
        container.gameObject.SetActive(!isEmpty);
    }

    public override void SetItemIndex(int contentIndex) { }
    public override void SetSelected(object dataObject) { }

    IEnumerator TypeMessage(string text)
    {
        int chrLeft = text.Length;
        while (chrLeft > 0)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.01f, 0.07f));
            chrLeft--;
            message.text = text.Substring(0, text.Length - chrLeft);
        }

        EventManager.Get<TypeMessageCompleteEvent>().Publish(data);
    }
}
