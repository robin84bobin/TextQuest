﻿using Assets.Scripts.UI.Windows;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Assets.Scripts;
using Assets.Scripts.UI;
using Assets.Scripts.Factories.DataFactories.JsonFactories;


public class CreateQuestMenuParams : WindowParams
{
    public BaseQuestStepData templateData;
    public Action<string> OnCreateSuccess = delegate { };
}


public class CreateQuestMenu : BaseWindow {

    [SerializeField] private Text _errorText;
    [SerializeField] private Dropdown _typeDropdown;
    [SerializeField] private InputField _inputField;

    CreateQuestMenuParams parameters;

    private string _newId;
    private string _newType;

    public static void Show(CreateQuestMenuParams params_)
    {
        Main.Inst.windows.Show("CreateQuestMenu", params_);
    }

    public override void OnShowComplete(WindowParams param_ = null)
    {
        base.OnShowComplete(param_);
        parameters = (CreateQuestMenuParams)windowsParameters;
        Init();
    }

    private void Init()
    {
        _typeDropdown.onValueChanged.RemoveAllListeners();
        _typeDropdown.onValueChanged.AddListener(OnTypeChanged);

        List<Dropdown.OptionData> optionsList = new List<Dropdown.OptionData>();
        optionsList.Add(new Dropdown.OptionData(QuestStepType.MESSAGE.ToString()));
        optionsList.Add(new Dropdown.OptionData(QuestStepType.TRIGGER.ToString()));
        _typeDropdown.ClearOptions();
        _typeDropdown.AddOptions(optionsList);

        //
        if (parameters.templateData != null)
        {   //нельзя менять тип если сохраняем уже существующий шаг
            _typeDropdown.SelectValue(parameters.templateData.type);
            _typeDropdown.interactable = false;
        }
        else
        {
            //_typeDropdown.gameObject.SetActive(true);
            _typeDropdown.interactable = true;
            _newType = _typeDropdown.options[0].text;
        }

    }

    private void OnTypeChanged(int val)
    {
        _newType = _typeDropdown.options[val].text;
    }

    public void OnSaveClick()
    {
        _newId = _inputField.text;

        if (string.IsNullOrEmpty(_newId))
        {
            _errorText.text = "new id is empty!";
            return;
        }

        if (Main.Inst.dataController.CheckDuplicateId<BaseQuestStepData>(_newId))
        {
            _errorText.text = "id is already exists!";
            return;
        }

        Save();
        Hide();
    }


    private void Save()
    {
        BaseQuestStepData item;
        if (parameters.templateData != null)
        {
            item = parameters.templateData;
            item.objectId = _newId;
        }
        else
        {
            item = CreateData();
            item.objectId = _newId;
            item.type = _newType;
        }
        
        Main.Inst.dataController.Set(item, _newId, true);
        //
        parameters.OnCreateSuccess.Invoke(_newId);
    }

    private BaseQuestStepData CreateData()
    {
        BaseQuestStepData item;
        switch (_newType)
        {
            case QuestStepType.MESSAGE:
                item = new QuestMessageStepData()
                {
                    variants = new QuestVariantData[] { }
                };
                break;
            case QuestStepType.TRIGGER:
                item = new QuestTriggerStepData();
                break;
            default: item = new BaseQuestStepData(); break;
        }
        return item;
    }

    public void OnCancelClick()
    {
        Hide();
    }

    protected override void OnHide()
    {
        base.OnHide();
        windowsParameters = null; //удаляем ссылку на параметр. т.к. там ссылка на колбек
    }
}

