﻿using UnityEngine;
using System.Collections;
using System;

abstract public class AbstractStepEditor : MonoBehaviour
{
    internal abstract void Init(BaseQuestStepData _questData);
    protected abstract void GrabDataFromUI();
    internal abstract BaseQuestStepData GetData();
    internal abstract void SaveData();
}
