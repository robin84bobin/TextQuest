﻿using System;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class CommonStepEditor : AbstractStepEditor {

    #region Components
    [SerializeField]    InputField _messageInput;
    [SerializeField]    VariantEditor[] _variants;
    #endregion

    private void ResetVariants()
    {
        for (int i = 0; i < _variants.Length; i++)
            _variants[i].ResetView();
    }

    string _id = string.Empty;
    internal override void Init(BaseQuestStepData _questData)
    {
        QuestMessageStepData sourceData = (QuestMessageStepData)_questData;
        _messageInput.text = sourceData.text;
        _id = sourceData.objectId;

        ResetVariants();
        for (int i = 0; i < _variants.Length; i++)
        {
            if (i < sourceData.variants.Length)
                _variants[i].SetData(sourceData.variants[i]);
        }
    }

    internal override void SaveData()
    {
        GrabDataFromUI();
        Main.Inst.dataController.Set(_data as BaseQuestStepData, _data.objectId, true);
    }

    internal override BaseQuestStepData GetData()
    {
        GrabDataFromUI();
        return _data;
    }

    QuestMessageStepData _data;
    protected override void GrabDataFromUI()
    {
        _data = new QuestMessageStepData();
        _data.objectId = _id;
        _data.text = _messageInput.text;
        int varCnt = 0;
        for (int i = 0; i < _variants.Length; i++)
        {
            var varData = _variants[i].GetData();
            if (varData != null && varData.targetStepId != "None")
            {
                varCnt++;
            }
            
        }

        _data.variants = new QuestVariantData[varCnt];
        for (int j = 0; j < varCnt; j++)
        {
            _data.variants[j] = _variants[j].GetData();
        }
    }


}
