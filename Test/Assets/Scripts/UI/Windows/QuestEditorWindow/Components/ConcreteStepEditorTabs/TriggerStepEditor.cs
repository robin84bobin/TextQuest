﻿using Assets.Scripts;
using UnityEngine;

public class TriggerStepEditor : AbstractStepEditor
{

    [SerializeField] TriggerEditor[] _triggers;

    string _id = string.Empty;
    internal override void Init(BaseQuestStepData data_)
    {
        QuestTriggerStepData sourceData = (QuestTriggerStepData)data_;
        _id = sourceData.objectId;
        for (int i = 0; i < _triggers.Length; i++)
        {
            QuestTriggerData triggerData = i < sourceData.triggers.Length ? sourceData.triggers[i] : null;
            _triggers[i].Init(triggerData);
        }
    }

    internal override void SaveData()
    {
        GrabDataFromUI();
        Main.Inst.dataController.Set(_data as BaseQuestStepData, _data.objectId, true);
    }

    internal override BaseQuestStepData GetData()
    {
        GrabDataFromUI();
        return _data as BaseQuestStepData;
    }

    QuestTriggerStepData _data;
    protected override void GrabDataFromUI()
    {
        _data = new QuestTriggerStepData();
        _data.objectId = _id;
        _data.triggers = new QuestTriggerData[_triggers.Length];
        for (int i = 0; i < _triggers.Length; i++)
        {
            _data.triggers[i] = _triggers[i].GetTriggerData();
        }
    }

}
