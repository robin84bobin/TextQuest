﻿using Assets.Scripts.Data.DataSource;
public class MessageData : BaseData
{
    public bool isNew;
    /// <summary>
    /// test of the message
    /// </summary>
    public string text;
    /// <summary>
    /// is the message send by user
    /// </summary>
    public bool isUserMsg;
    /// <summary>
    /// id of quest step which has created the message
    /// </summary>
    public string parentQuestStepId;


}
