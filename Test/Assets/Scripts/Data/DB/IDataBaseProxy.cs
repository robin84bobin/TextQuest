using System;
using System.Collections.Generic;
using Assets.Scripts.Data.DataSource;

namespace Assets.Scripts.Data.DB
{
    public interface IDataBaseProxy
    {
        void Init();
        double LastUpdateTime (string tableName_);
        bool IsTableExist(string tableName_);
     //   void SaveTableData<TBaseData>(string tableName_, Dictionary<string, TBaseData> dataDictionary_) where TBaseData:IBaseData;
        void SaveTableData<T>(string tableName_, Dictionary<string, T> dataDictionary_) where T : IBaseData;
        void GetTableData<TBaseData> (string tableName_, Action<string, Dictionary<string, TBaseData>> callback_) where TBaseData:IBaseData, new();
        void CreateTable<T>(string tableName_);
    }
}


