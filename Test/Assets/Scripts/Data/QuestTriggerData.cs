﻿/// <summary>
/// types of conditions to switch quest trigger
/// </summary>

public class QuestTriggerData
{
    /// <summary>
    /// condition to switch quest trigger
    /// </summary>
    public string condition;
    /// <summary>
    ///  Id of trigger step 
    /// </summary>
    public string triggerStepId;
    /// <summary>
    /// Id of step to go if condition is true
    /// </summary>
    public string targetStepId;
    /// <summary>
    /// Id of step to go if condition is false
    /// </summary>
    public string alterStepId;
}
