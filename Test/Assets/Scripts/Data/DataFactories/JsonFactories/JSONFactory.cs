﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Data.DataSource;
using UnityEngine;

namespace Assets.Scripts.Factories.DataFactories.JsonFactories
{
    public class JsonFactory
    {
        private static JsonFactory _instance;
        public static JsonFactory Instance
        {
            get { return _instance ?? (_instance = new JsonFactory()); }
        }

        private readonly Dictionary<Type, AbstractJsonFactory> _factories;

        private JsonFactory()
        {
            _factories = new Dictionary<Type, AbstractJsonFactory> {
                {typeof (BaseQuestStepData), new QuestStepJsonFactory()},
                {typeof (UserQuestStepData), new UserQuestStepJsonFactory()}
             /* {typeof (SkillImpactData), new SkillImpactJsonFactory()},
                {typeof (PeriodImpactData), new PeriodImpactJsonFactory()},
                {typeof (BehaviourImpactData), new BehaviourImpactJsonFactory()},
                {typeof (HeroData), new HeroJsonFactory()},
                {typeof (EnemyData), new EnemyJsonFactory()},*/
            };

        }

        public T Create<T>( string jsonString_) where T : IBaseData, new()
        {
            if (_factories.ContainsKey(typeof(T))) {
                return (T)_factories[typeof(T)].Create(jsonString_) ;
            }
            else {
                return DefaultJsonFactory.Create<T>( jsonString_);
            }
        }

        public string ParseData<T>(T data) where T : IBaseData
        {
            if (_factories.ContainsKey(typeof(T)))
            {
                return _factories[typeof(T)].ParseData(data);
            }
            else {
                return DefaultJsonFactory.ParseData<T>(data);
            }
        }
    }



    internal static class DefaultJsonFactory
    {
        public static T Create<T>( string jsonString_) where T : new () 
        {
           T data = new T();
           JsonUtility.FromJsonOverwrite(jsonString_, data);
           return data;
        }

        public static string ParseData<T>(T data) where T : IBaseData
        {
            return JsonUtility.ToJson(data);
        }
    }
}
