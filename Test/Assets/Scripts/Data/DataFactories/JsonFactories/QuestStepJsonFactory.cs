﻿using Assets.Scripts.Factories.DataFactories.JsonFactories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Data.DataSource;
using Assets.JSON;
using UnityEngine;
using SimpleJSON;

public class QuestStepJsonFactory : AbstractJsonFactory
{
    private readonly Dictionary<string, Func<string, BaseQuestStepData>> _createActions;
    private readonly Dictionary<string, Func<BaseQuestStepData, string>> _parseActions;

    public QuestStepJsonFactory()
    {
        _createActions = new Dictionary<string, Func<string, BaseQuestStepData>>
        {
            {"message", CreateMessage },
            {"trigger", CreateTrigger }
        };

        _parseActions = new Dictionary<string, Func<BaseQuestStepData, string>>
        {
            {"message", ParseMessage },
            {"trigger", ParseTrigger }
        };
    }


    #region create
    public override IBaseData Create(string jsonString_)
    {
        BaseQuestStepData baseData = DefaultJsonFactory.Create<BaseQuestStepData>(jsonString_); 
        if(_createActions.ContainsKey(baseData.type))
        {
            baseData =  _createActions[baseData.type].Invoke(jsonString_);
        }
        return baseData ;
    }

    private QuestMessageStepData CreateMessage(string jsonString_)
    {
        QuestMessageStepData data = DefaultJsonFactory.Create<QuestMessageStepData>(jsonString_);
        //TODO parse complex fileds
        JSONNode json = JSON.Parse(jsonString_);
        //variants
        JSONNode variants = json["variants"];
        data.variants = new QuestVariantData[variants.Count];
        for (int i = 0; i < variants.Count; i++)
        {
            data.variants[i] = DefaultJsonFactory.Create<QuestVariantData>(variants[i].ToString());
            data.variants[i].variantId = i;
            data.variants[i].parentStepId = data.ObjectId;
        }
        //
        return data;
    }

    private QuestTriggerStepData CreateTrigger(string jsonString_)
    {
        QuestTriggerStepData data = DefaultJsonFactory.Create<QuestTriggerStepData>(jsonString_);
        //TODO parse complex fileds
        JSONNode json = JSON.Parse(jsonString_);
        //triggers
        JSONNode triggers = json["triggers"];
        data.triggers = new QuestTriggerData[triggers.Count];
        for (int i = 0; i < triggers.Count; i++)
        {
            data.triggers[i] = DefaultJsonFactory.Create<QuestTriggerData>(triggers[i].ToString());
        }
        //
        return data;
    }
    #endregion

    #region parse 
    public override string ParseData(IBaseData data_)
    {
        BaseQuestStepData data = (BaseQuestStepData)data_;
        string result = DefaultJsonFactory.ParseData(data);
        if (_parseActions.ContainsKey(data.type))
        {
            result = _parseActions[data.type].Invoke(data);
        }
        return result;
    }

    private string ParseTrigger(BaseQuestStepData data_)
    {
        QuestTriggerStepData data = (QuestTriggerStepData)data_;

        JSONClass root = new JSONClass();
        root["type"] = data.type;
        root["dataType"] = data.dataType;
        root["objectId"] = data.objectId;

        JSONClass triggers =  new JSONClass();
        for (int i = 0; i < data.triggers.Length; i++)
        {
            QuestTriggerData triggerData = data.triggers[i];
            JSONClass triggerNode = new JSONClass();
            triggerNode["condition"] = triggerData.condition;
            triggerNode["triggerStepId"] = triggerData.triggerStepId;
            triggerNode["targetStepId"] = triggerData.targetStepId;
            triggerNode["alterStepId"] = triggerData.alterStepId;
            triggers.Add(triggerNode);
        }
        root["triggers"] = triggers;
        string result = root.ToString();
        
        return result;
    }

    private string ParseMessage(BaseQuestStepData data_)
    {
        QuestMessageStepData data = (QuestMessageStepData)data_;
        
        JSONClass root = new JSONClass();
        root["type"] = data.type;
        root["dataType"] = data.dataType;
        root["objectId"] = data.objectId;
        root["text"] = data.text;
        root["delayAfterStep"] = data.delayAfterStep.ToString();
        root["delayAfterStepMessage"] = data.delayAfterStepMessage.ToString();

        JSONClass variants = new JSONClass();
        for (int i = 0; i < data.variants.Length; i++)
        {
            JSONClass varintNode = new JSONClass();
            QuestVariantData variantData = data.variants[i];
            //varintNode["variantId"] = variantData.variantId.ToString();
            varintNode["parentStepId"] = data.objectId;// variantData.parentStepId ;
            varintNode["targetStepId"] = variantData.targetStepId ;
            varintNode["text"] = variantData.text ;
            variants.Add((i+1).ToString(), varintNode);

        }
        root["variants"] = variants;
        string result = root.ToString();
        
        return result;
    }
    #endregion
}

