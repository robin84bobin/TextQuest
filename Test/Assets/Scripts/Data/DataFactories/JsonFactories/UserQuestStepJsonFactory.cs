﻿using Assets.Scripts.Data.DataSource;
using Assets.Scripts.Factories.DataFactories.JsonFactories;
using SimpleJSON;

public class UserQuestStepJsonFactory : AbstractJsonFactory
{
    public override IBaseData Create(string jsonString_)
    {
        UserQuestStepData data = DefaultJsonFactory.Create<UserQuestStepData>(jsonString_);
        JSONNode root = JSON.Parse(jsonString_);
        JSONNode messages = root["messages"];
      /*  for (int i = 0; i < data.messagesToShow.Count; i++)
        {
            MessageData messageData = new MessageData( 
                messages[i]["text"],
                messages[i]["isUser"].AsBool );
            data.messagesToShow.Add(messageData);
        }*/
        return data;
    }

    public override string ParseData(IBaseData data_)
    {
        UserQuestStepData data = (UserQuestStepData)data_;
        string result;

        JSONClass root = new JSONClass();
        root["dataType"] =          new JSONData(data.dataType);
        root["objectId"] =          new JSONData(data.objectId);
        root["questStepId"] =       new JSONData(data.questStepId);
        root["state"] =             new JSONData(data.state);
        root["chosenVariantId"] =   new JSONData(data.chosenVariantId);
        JSONClass messages = new JSONClass();
     /*   for (int i = 0; i < data.messagesToShow.Count; i++)
        {
            MessageData messageData = data.messagesToShow[i];
            JSONClass messageNode = new JSONClass();
            messageNode["text"] = new JSONData(messageData.text);
            messageNode["isUser"] = new JSONData(messageData.isUserMsg);
            messages[i.ToString()] = messageNode;
        }
        root.Add("messages", messages);*/
        result = root.ToString();
        return result;
    }
}