﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Data.DataSource;
using Assets.Scripts.Events;
using Assets.Scripts.Events.CustomEvents;
using Assets.Scripts.Network;
using UnityEngine;

namespace Assets.Scripts.Data.DataStorages
{
    public interface IBaseStorage
    {
        string DataTypeName {get;}
        IBaseData Get(string objectId_ );
        void LoadData();
    }

    public class BaseStorage<T> : IBaseStorage where T : IBaseData, new()
    {
        public int Count { get { return items.Count; } }

        protected readonly bool readOnly;
        protected Dictionary<string, T> items;

        protected string dataType;

        public BaseStorage(string dataType_, bool readOnly_ = true)
        {
            readOnly = readOnly_;
            this.dataType = dataType_;
            items = new Dictionary<string, T>();
        }

        public string DataTypeName {
            get {
                return dataType;
            }
        }

        public void LoadData()
        {
            EventManager.Get<LoadProgressEvent>().Publish(GetLoadMessage());
            DataProxy.Instance.LoadData<T>(dataType, OnLoadDataComplete);
        }

        public IEnumerable<T> GetItemsList()
        {
            return items.Values;
        }

        protected void OnLoadDataComplete(string tableName_, Dictionary<string, T> objects_)
        {
            Debug.Log(string.Format("LOADED DATA: {0} ", dataType));
            items = objects_ ?? new Dictionary<string, T>();
            Log(this.items);
            EventManager.Get<StorageLoadCompleteEvent>().Publish();
        }

        internal void Clear(bool saveNow = true)
        {
            items.Clear();
            if (saveNow)
            {
                SaveData();
            }
        }

        public void SaveData()
        {
            if (readOnly) {
                Debug.LogError("Can't write into read only storage: " + GetType().Name);
                return;
            }
            DataProxy.Instance.LocalDb.SaveTableData(dataType, items);

            //EventManager.Get<StorageUpdatedEvent>().Publish(typeof(T));
            Main.Inst.dataController.OnStorageUpdated(typeof(T));
        }

        public void Remove(string objectId_, bool saveNow = false)
        {
            if (!items.ContainsKey(objectId_))
            {
                Debug.LogError(string.Format("Can't remove data from '{0}' storage - there is NO objectId:{1}", dataType, objectId_));
                return;
            }

            //КОСТЫЛЬ!
            //вызываем событие ДО того как фактически удалим шаг
            //т.к. при переключении редактора сохраняется отображаемый шаг (т.е. тот, что мы удаляем)
            //и если вызвать событие после, то он снова появится в сторадже
            EventManager.Get<RemoveQuestStepEditorEvent>().Publish(objectId_);

            items.Remove(objectId_);
            
            if (saveNow) SaveData();
        } 

        public bool Check(string objectId_)
        {
            return items.ContainsKey(objectId_);
        }

        public T Get (string objectId_)
        {
            if (!items.ContainsKey(objectId_)){
                Debug.LogError(string.Format("Can't get data from '{0}' storage - objectId:{1}", dataType ,objectId_));
                return default(T);
            }

            return items[objectId_];
        }

        public void Set(T item, string objectId_, bool saveNow = false)
        {
            if (items.ContainsKey(objectId_))
            {
                items[objectId_] =  item;
                Debug.Log(string.Format("Replace data in '{0}' storage eg. objectId:{1} already exists", dataType, objectId_));
            }
            else
            {
                items.Add(objectId_, item);
                Debug.Log(string.Format("Add data in '{0}' storage  objectId:{1}", dataType, objectId_));
            }

            if (saveNow) SaveData();
        }

        public T Get (Func<T,bool> predicate_)
        {
            return items.Values.FirstOrDefault(predicate_);
        }

        void Log(Dictionary<string,T> dict_)
        {
            foreach (var item in dict_) {
                Debug.Log(string.Format(" {0} > {1}", item.Value.DataType, item.Value.ObjectId));
            }
        }

        string GetLoadMessage ()
        {
            return string.Format("Loading: \"{0}\" ...", dataType);
        }

        string GetUpdateMessage()
        {
            return string.Format("Updating: \"{0}\" ...", dataType);
        }

        IBaseData IBaseStorage.Get(string objectId_)
        {
            if (!items.ContainsKey(objectId_))
            {
                Debug.LogError(string.Format("Can't get data from '{0}' storage - objectId:{1}", dataType, objectId_));
                return default(T);
            }

            return items[objectId_];
        }
    }
}