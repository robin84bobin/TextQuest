using System;
using Assets.Scripts.Data.Attributes;

namespace Assets.Scripts.Data.DataSource
{
    public interface IBaseData
    {
        /// <summary>
        /// Defines table name
        /// </summary>
        string DataType { get; set; }
        /// <summary>
        /// pass-through id
        /// </summary>
        string ObjectId { get;}
    }

    public class BaseData : IBaseData
    {
        [DbField]
        public string dataType = string.Empty;
        [DbField]
        public string objectId = string.Empty;

        public string DataType
        {
            get { return dataType; }
            set { dataType = value; }
        }

        public string ObjectId
        {
            get { return objectId; } 
        }
    }
}