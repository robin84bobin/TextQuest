﻿using Assets.Scripts;
using Assets.Scripts.Data.DataSource;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Contains info about steps user had passed
/// </summary>
public class UserQuestStepData : BaseData
{
    /// <summary>
    /// Quest step id
    /// </summary>
    public string questStepId;
    /// <summary>
    /// State of quest step
    /// </summary>
    public string state;
    /// <summary>
    /// variant chosen by user
    /// </summary>
    public int chosenVariantId;
    /// <summary>
    /// messages to show.
    /// could contains messages of quest step and
    /// message of user chosen variant
    /// </summary>
    //public List<MessageData> messagesToShow = new List<MessageData>();

    public void Complete(int variantId)
    {
        state = UserQuestState.COMPLETE;
        chosenVariantId = variantId;
        Main.Inst.dataController.Set(this, questStepId);
    }

    public QuestMessageStepData GetQuestStep()
    {
        return
            Main.Inst.dataController.Get<BaseQuestStepData>(questStepId)
            as QuestMessageStepData;
    }
}
