﻿public class QuestTriggerStepData : BaseQuestStepData
{
    public QuestTriggerStepData()
    {
        type = QuestStepType.TRIGGER;
    }

    public QuestTriggerData[] triggers = new QuestTriggerData[] { };
}
