namespace Assets.Scripts.Data
{
    public struct DataTypes
    {
        public const string STEPS = "steps";
        public const string USER_STEPS = "user_steps";
        public const string MESSAGES = "messages";
    }
}


