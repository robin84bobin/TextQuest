using System;
using System.Collections.Generic;
using Assets.Scripts.Data.DataSource;
using Assets.Scripts.Data.DataStorages;
using Assets.Scripts.Data.User;
using Assets.Scripts.Events;
using Assets.Scripts.Events.CustomEvents;
using Assets.Scripts.Network;
using UnityEngine;

namespace Assets.Scripts.Data
{
    public sealed class DataController
    {
        #region EVENTS

        public event Action<Type> onStorageUpdated = delegate { };
        public void OnStorageUpdated(Type type) { onStorageUpdated.Invoke(type); }

        public event Action<Type, string> onItemRemoved = delegate { };
        public void OnItemRemoved(Type type, string id) { onItemRemoved.Invoke(type, id); }

        #endregion

        //------------------
        private Dictionary<Type, IBaseStorage> _storageTypeMap;
        private Dictionary<string, IBaseStorage> _storageTypeNameMap;
        //------------------


        public void Init ()
        {
            EventManager.Get<StorageLoadCompleteEvent> ().Subscribe (LoadNextStorage);
            EventManager.Get<StorageUpdateCompleteEvent> ().Subscribe (LoadStorage);
            DataProxy.Instance.Init ();
            InitStorages();
            StartLoadStorages ();
        }


        private void InitStorages()
        {
            _storageTypeMap = new Dictionary<Type, IBaseStorage>();
            _storageTypeNameMap = new Dictionary<string, IBaseStorage>();

            //
            RegisterBaseStorage<BaseQuestStepData>(DataTypes.STEPS, false);
            //user storages...
            RegisterBaseStorage<MessageData>(DataTypes.MESSAGES, false);
            RegisterBaseStorage<UserQuestStepData>(DataTypes.USER_STEPS, false);
        }

        public void ClearUserData()
        {
            Main.Inst.dataController.Storage<MessageData>().Clear();
            Main.Inst.dataController.Storage<UserQuestStepData>().Clear();
        }

        public IBaseStorage StorageByName(string typeName_)
        {
            if (_storageTypeNameMap.ContainsKey(typeName_))
            {
                return ((IBaseStorage)_storageTypeNameMap[typeName_]);
            }
            Debug.LogError("Storage not found for data type: " + typeName_);
            return null;
        }

        public BaseStorage<T> Storage<T>() where T:BaseData, new()
        {
            Type type = typeof(T);
            if (_storageTypeMap.ContainsKey(type)){
                return ((BaseStorage<T>)_storageTypeMap[type]);
            }
            Debug.LogError("Storage not found for data type: "+ type.Name);
            return null;
        }


        public void Set<T>(T item, string objectId_ = "", bool saveNow = true) where T : BaseData, new()
        {
            if (string.IsNullOrEmpty(objectId_))
            {
                objectId_ = DateTime.Now.Millisecond.ToString() + "_" + UnityEngine.Random.Range(0f, 1000f).ToString();
            }
            item.objectId = objectId_;
            Storage<T>().Set(item, objectId_, saveNow);
        }

        public IBaseData Get(string dataType_, string objectId_)
        {
            return StorageByName(dataType_).Get(objectId_);
        }


        public T Get<T>(string objectId_) where T:BaseData, new() 
        {
            return Storage<T>().Get(objectId_);
        }


        public T Get<T>(Func<T,bool> predicate_) where T:BaseData, new() 
        {
            return Storage<T>().Get(predicate_);
        }


        private BaseStorage<T> RegisterBaseStorage<T>(string name_, bool readonly_ = true) where T:BaseData, new()
        {
            BaseStorage<T> storage = new BaseStorage<T>(name_,readonly_);
            _storageTypeMap.Add(typeof(T),storage);
            _storageTypeNameMap.Add(name_, storage);

            if (!readonly_) 
            {
               // storage.SaveData(); // create file
            }

            return storage;
        }


        int _nextStorageIndex = -1;
        private Queue<IBaseStorage> _storageLoadQueue;
        void StartLoadStorages ()
        {
            _nextStorageIndex = 0;
            _storageLoadQueue = new Queue<IBaseStorage>( _storageTypeMap.Values);
            LoadNextStorage();
        }


        void OnStoragesUpdateComplete()
        {
            EventManager.Get<StorageLoadCompleteEvent> ().Unsubscribe (LoadNextStorage);
            EventManager.Get<StorageUpdateCompleteEvent> ().Unsubscribe (LoadStorage);
            EventManager.Get<DataInitCompleteEvent> ().Publish ();
        }


        void LoadNextStorage ()
        {
            if ( _storageLoadQueue.Count <= 0){
                OnStoragesUpdateComplete();
                return;
            }

            IBaseStorage currentStorage = _storageLoadQueue.Dequeue();
            if(currentStorage == null){
                LoadNextStorage();
                return;
            }

            currentStorage.LoadData();
        }


        void LoadStorage (Type dataType_)
        {
            if (!_storageTypeMap.ContainsKey(dataType_)){
                Debug.Log("Data storage does not exist:" + dataType_.Name);
                return;
            }
            _storageTypeMap[dataType_].LoadData();
        }


        public bool CheckDuplicateId<T>(string newId_) where T:BaseData, new ()
        {
            return Storage<T>().Check(newId_);
        }
    }
}


