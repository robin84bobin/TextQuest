﻿using Assets.Scripts.Data.DataSource;
public class BaseQuestStepData : BaseData
{
    /// <summary>
    /// type of step
    /// </summary>
    public string type;
}
