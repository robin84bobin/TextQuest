using System;
using Assets.Scripts.Controllers;
using Assets.Scripts.Data;
using Assets.Scripts.Events;
using Assets.Scripts.Events.CustomEvents;
using Assets.Scripts.UI;
using Parse;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public sealed class Main : MonoBehaviour 
    {
        //----------------------------------
        public WindowManager windows;
        public GameManager game;
		//----------------------------------
        //----------------------------------

        public DataController dataController { get; private set; }
        public UserStepsController userStepsController { get; private set; }

        private static Main _instance;
        public static Main Inst {
            get {
                return _instance;
            }
        }

        private void Awake()
        {
            _instance = this;
        }

        void Start()
        {
            _instance = this;

            //DontDestroyOnLoad(gameObject);
            Init ();
        }

        void Init ()
        {
            EventManager.Get<DataInitCompleteEvent> ().Subscribe (OnDataInited);
            SceneManager.activeSceneChanged += OnSceneChanged;
            //
            
            //PreloaderWindow.Show();
            InitData();
            Scene scene = SceneManager.GetActiveScene();
            InitInScene(scene);
        }

        private void InitInScene(Scene scene)
        {
            if (scene == SceneManager.GetSceneByName("QuestEditor"))
            {
                //QuestEditorWindow.Show();
            }
        }

        private void OnSceneChanged(Scene arg0, Scene arg1)
        {
            InitInScene(arg1);
        }

        void InitData()
        {
            //first init dataController 
            dataController = new DataController();
            dataController.Init ();

            userStepsController = new UserStepsController();
            userStepsController.Init();
        }

        void OnDataInited ()
        {


            EventManager.Get<DataInitCompleteEvent> ().Unsubscribe (OnDataInited);
            Debug.Log ("COMPLETE!");
           // _dataController.StartUserSession();
        }


        public void OnSwitchToEditorClick()
        {
            SceneManager.LoadScene("QuestEditor");
        }
    }
}
