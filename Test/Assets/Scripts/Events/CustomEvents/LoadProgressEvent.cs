namespace Assets.Scripts.Events.CustomEvents
{
    public sealed class LoadProgressEvent : GameParamEvent<string> {}
}
