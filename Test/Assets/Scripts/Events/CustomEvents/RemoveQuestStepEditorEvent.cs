﻿namespace Assets.Scripts.Events.CustomEvents
{
    public sealed class RemoveQuestStepEditorEvent : GameParamEvent<string> { }
}
