﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Events;

public class NewMessageEvent : GameParamEvent<MessageData>
{
}

public class TypeMessageCompleteEvent : GameParamEvent<MessageData> { }
