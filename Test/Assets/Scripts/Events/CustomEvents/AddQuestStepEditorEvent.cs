﻿namespace Assets.Scripts.Events.CustomEvents
{
    public sealed class AddQuestStepEditorEvent : GameParamEvent<string> { }
}
