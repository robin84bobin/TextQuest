﻿using Assets.Scripts;
using Assets.Scripts.Data;
using Assets.Scripts.Data.DataStorages;
using Assets.Scripts.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


/// <summary>
/// class controls steps which user had passed
/// </summary>
public class UserStepsController
{
    public string activeStepId { get; private set; }

    public event Action<UserQuestStepData> OnStepComplete = delegate { };

    private DataController _data;
    private BaseStorage<UserQuestStepData> _userStepStorage;


    public void Init()
    {
        _data = Main.Inst.dataController;
        _userStepStorage = _data.Storage<UserQuestStepData>();
    }

    public void GoToStep(string questId, string state = UserQuestState.ACTIVE, int variantId = -1)
    {
        BaseQuestStepData questStepData = _data.Get<BaseQuestStepData>(questId);

        if (questStepData == null)
            return;

        if (questStepData.type == QuestStepType.TRIGGER)
        {
            QuestTriggerStepData triggerStep = (QuestTriggerStepData)questStepData;
            CheckTrigger(triggerStep);
            return;
        }

        if (questStepData.type == QuestStepType.MESSAGE)
        {
            QuestMessageStepData messageStep = (QuestMessageStepData)questStepData;
            UserQuestStepData userStep = new UserQuestStepData();
            userStep.questStepId = questId;
            userStep.state = state;
            
            CreateMessage(questId, messageStep.text);

            if (variantId > 0)
            {
                CompleteStep(questId, variantId);
            }
            //_userStepStorage.Set(userStep, questId , true);

            Main.Inst.dataController.Set(userStep, questId);
        }
        
    }

    public void CompleteStep(string questId, int variantId)
    {
        UserQuestStepData userStep = _userStepStorage.Get(questId);
        if (userStep.state != UserQuestState.ACTIVE)
        {
            Debug.LogError("Failed to complete step! Tried to complete inactive userStep id=" + questId);
            return;
        }

        BaseQuestStepData questStepData = Main.Inst.dataController.Get<BaseQuestStepData>(questId);
        if (questStepData.type == QuestStepType.MESSAGE)
        {
            QuestMessageStepData messageStep = (QuestMessageStepData)questStepData;
            if (variantId < messageStep.variants.Length)
            {
                QuestVariantData variant = messageStep.variants[variantId];
                //Complete step
                userStep.Complete(variantId);
                //Create message with variant text
                CreateMessage(questId, variant.text, true);
                //Go to target step
                GoToStep(variant.targetStepId);
            }
            else
            {
                Debug.LogError("Failed to complete step! No variant id=" + variantId.ToString() + " found in step id=" + messageStep.ObjectId);
                return;
            }
        }
       
        //OnStepComplete.Invoke(userStep);
    }

    private void CheckTrigger(QuestTriggerStepData triggerStep)
    {
        QuestTriggerData trigger;
        int length = triggerStep.triggers.Length;
        for (int i = 0; i < length; i++) // looking for step to which trigger leads to
        {
            trigger = triggerStep.triggers[i];
            UserQuestStepData userStep = _userStepStorage.Get(u => u.questStepId == trigger.triggerStepId);
            //check trigger condition 
            if (trigger.condition == QuestTriggerCondition.COMPLETE)
            {
                if (userStep != null)
                {
                    GoToStep(trigger.targetStepId); break;
                }
                else
                if (!string.IsNullOrEmpty(trigger.alterStepId))
                {
                    GoToStep(trigger.alterStepId); break;
                }
            }
            else
            if (trigger.condition == QuestTriggerCondition.UNCOMPLETE)
            {
                if (userStep == null)
                {
                    GoToStep(trigger.targetStepId); break;
                }
                else
                if (!string.IsNullOrEmpty(trigger.alterStepId))
                {
                    GoToStep(trigger.alterStepId); break;
                }
            }
        }
    }

    public UserQuestStepData GetActiveStep()
    {
        UserQuestStepData activeStep = _userStepStorage.Get(s => s.state == UserQuestState.ACTIVE);
        activeStepId = activeStep.questStepId;
        return activeStep;
    }

    private void CreateMessage(string parentQuestStepId, string text, bool isUserMsg = false)
    {
        MessageData msg = new MessageData();
        msg.text = text;
        msg.isUserMsg = isUserMsg;
        msg.parentQuestStepId = parentQuestStepId;

        Main.Inst.dataController.Set(msg);
        EventManager.Get<NewMessageEvent>().Publish(msg);
    }
}
