﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommandManager
{
    #region Singleton
    static CommandManager _instance;
    public static CommandManager Instance
    {
        get
        {
            if (_instance == null) _instance = new CommandManager();
            return _instance;
        }
    }
    #endregion

    List<Command> commandList = new List<Command>();

    public void Undo() { }
    public void Redo() { }

}
